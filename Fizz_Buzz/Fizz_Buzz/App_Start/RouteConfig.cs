﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Fizz_Buzz
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "FizzBuzz",
               url: "FizzBuzz/GetFizzBuzz",
               defaults: new { controller = "FizzBuzz", action = "GetFizzBuzz", id = UrlParameter.Optional }
           );
        }
    }
}
