﻿using FizzBuzz.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;


namespace Fizz_Buzz.Models
{
    public class FizzBuzzModel
    {
        public FizzBuzzModel()
        { }

        [Required]
        [Range(1, 1000, ErrorMessage = "Only positive number allowed")]
        public int FizzBuzzCount { get; set; }

        public IEnumerable<IFizzBuzzItem> GetFizzBuzz()
        {
            List<IFizzBuzzItem> fizzBuzz = new List<IFizzBuzzItem>();
            int i;
            bool isWednesday = false;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                isWednesday = true;
            }
            for (i = 1; i <= FizzBuzzCount; i++)
            {
                bool isFizz = false;
                bool isBuzz = false;
                IFizzBuzzItem item = null;
                if (i % 3 == 0)
                {
                    if (isWednesday)
                    {
                        item = new FizzBuzzItem(i, "Wizz", Color.Blue);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "Fizz", Color.Blue);
                    }
                    isFizz = true;
                }
                if (i % 5 == 0)
                {
                    if (isWednesday)
                    {
                        item = new FizzBuzzItem(i, "Wuzz", Color.Green);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "Buzz", Color.Green);
                    }
                    isBuzz = true;
                }
                if (isFizz && isBuzz)
                {
                    if (isWednesday)
                    {
                        item = new FizzBuzzItem(i, "WizzWuzz", Color.Green);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "FizzBuzz", Color.Black);
                    }
                }
                if (!isFizz && !isBuzz)
                {
                    item = new FizzBuzzItem(i, i.ToString(), Color.Black);
                }
                fizzBuzz.Add(item);
            }
            return fizzBuzz.AsEnumerable<IFizzBuzzItem>();
        }
    }
}