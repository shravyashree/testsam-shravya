﻿using System.Drawing;

namespace Fizz_Buzz.Models
{
    public interface IFizzBuzzItem
    {
        string Text { get; }
        Color Color { get; }

    }
}