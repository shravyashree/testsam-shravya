﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using Fizz_Buzz.Models;

namespace FizzBuzz.Models
{
    public class FizzBuzzItem : IFizzBuzzItem
    {
        public FizzBuzzItem(int fizzBuzzNumber, string displayText, Color color)
        {
            Text = displayText;
            this.Color = color;
        }

        public string Text { get;  set; }
        public Color Color { get;  set; }       
    }
}