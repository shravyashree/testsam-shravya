﻿using Fizz_Buzz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fizz_Buzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        // GET: FizzBuzz
        public ActionResult Index()
        {
            return View("DefaultView");
        }
        [HttpPost]
        public ActionResult GetFizzBuzz(FizzBuzzModel FizzBuzzModel)
        {
            IEnumerable< IFizzBuzzItem> fizzBuzzItems = null;
            fizzBuzzItems = FizzBuzzModel.GetFizzBuzz();
            ViewBag.FizzBuzzCount = FizzBuzzModel.FizzBuzzCount;
            return View("FizzBuzzView", fizzBuzzItems);
        }
    }
}